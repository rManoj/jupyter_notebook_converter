

```python
import pandas as pd
```

# Tables


```python
sample={"key1":["a","b","c","d","e"],"key2":["a","b","c","d","e"],"key3":["a","b","c","d","e"],"key4":[1,2,3,4,5],"key5":[-1,-2,-3,-4,-5]}
```


```python
df=pd.DataFrame(sample)
```


```python
df
```




<div>
<style>
    .dataframe thead tr:only-child th {
        text-align: right;
    }

    .dataframe thead th {
        text-align: left;
    }

    .dataframe tbody tr th {
        vertical-align: top;
    }
</style>
<table border="1" class="dataframe">
  <thead>
    <tr style="text-align: right;">
      <th></th>
      <th>key1</th>
      <th>key2</th>
      <th>key3</th>
      <th>key4</th>
      <th>key5</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th>0</th>
      <td>a</td>
      <td>a</td>
      <td>a</td>
      <td>1</td>
      <td>-1</td>
    </tr>
    <tr>
      <th>1</th>
      <td>b</td>
      <td>b</td>
      <td>b</td>
      <td>2</td>
      <td>-2</td>
    </tr>
    <tr>
      <th>2</th>
      <td>c</td>
      <td>c</td>
      <td>c</td>
      <td>3</td>
      <td>-3</td>
    </tr>
    <tr>
      <th>3</th>
      <td>d</td>
      <td>d</td>
      <td>d</td>
      <td>4</td>
      <td>-4</td>
    </tr>
    <tr>
      <th>4</th>
      <td>e</td>
      <td>e</td>
      <td>e</td>
      <td>5</td>
      <td>-5</td>
    </tr>
  </tbody>
</table>
</div>



# Visualization


```python
import matplotlib.pyplot as plt
```


```python
import numpy as np
```


```python
import seaborn as sns
from scipy import stats
```


```python
sns.set(color_codes=True)
```


```python
x = np.random.normal(0, 1, size=30)
bandwidth = 1.06 * x.std() * x.size ** (-1 / 5.)
support = np.linspace(-4, 4, 200)

kernels = []
for x_i in x:

    kernel = stats.norm(x_i, bandwidth).pdf(support)
    kernels.append(kernel)
    plt.plot(support, kernel, color="r")

sns.rugplot(x, color=".2", linewidth=3)
plt.show()
```


![png](output_10_0.png)



```python
sns.kdeplot(x, shade=True);
plt.show()
```


![png](output_11_0.png)

