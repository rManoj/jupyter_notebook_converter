

```python
import pandas as pd
```


```python
!pip install tabulate
```

    Collecting tabulate
      Downloading https://files.pythonhosted.org/packages/c4/f4/770ae9385990f5a19a91431163d262182d3203662ea2b5739d0fcfc080f1/tabulate-0.8.7-py3-none-any.whl
    Installing collected packages: tabulate
    Successfully installed tabulate-0.8.7
    

    You are using pip version 9.0.3, however version 20.0.2 is available.
    You should consider upgrading via the 'python -m pip install --upgrade pip' command.
    


```python
from tabulate import tabulate
```

# Tables


```python
sample={"key1":["a","b","c","d","e"],"key2":["a","b","c","d","e"],"key3":["a","b","c","d","e"],"key4":[1,2,3,4,5],"key5":[-1,-2,-3,-4,-5]}
```


```python
df=pd.DataFrame(sample)
```


```python
print(tabulate(df, tablefmt="pipe", headers="keys"))
```

    |    | key1   | key2   | key3   |   key4 |   key5 |
    |---:|:-------|:-------|:-------|-------:|-------:|
    |  0 | a      | a      | a      |      1 |     -1 |
    |  1 | b      | b      | b      |      2 |     -2 |
    |  2 | c      | c      | c      |      3 |     -3 |
    |  3 | d      | d      | d      |      4 |     -4 |
    |  4 | e      | e      | e      |      5 |     -5 |
    

# Visualization


```python
import matplotlib.pyplot as plt
```


```python
import numpy as np
```


```python
import seaborn as sns
from scipy import stats
```


```python
sns.set(color_codes=True)
```


```python
x = np.random.normal(0, 1, size=30)
bandwidth = 1.06 * x.std() * x.size ** (-1 / 5.)
support = np.linspace(-4, 4, 200)

kernels = []
for x_i in x:

    kernel = stats.norm(x_i, bandwidth).pdf(support)
    kernels.append(kernel)
    plt.plot(support, kernel, color="r")

sns.rugplot(x, color=".2", linewidth=3)
plt.show()
```


![png](output_12_0.png)



```python
sns.kdeplot(x, shade=True);
plt.show()
```


![png](output_13_0.png)

